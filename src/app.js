// Help taken from http://www.dappuniversity.com/articles/blockchain-app-tutorial
App = {
    loading: false,
    contracts: {},

    load: async () => {
        await App.loadWeb3();
        await App.loadAccount();
        await App.loadContract();
        await App.watchNewItemCreatedEvent ();
        await App.watchNewUserAdded ();
        await App.watchItemTransferRequested ();
        await App.watchItemTransferAccepted ();
        await App.watchItemTransferRejected ();
        App.render();
    },

    // https://medium.com/metamask/https-medium-com-metamask-breaking-change-injecting-web3-7722797916a8
    loadWeb3: async () => {
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider;
            web3 = new Web3(web3.currentProvider);
        } else {
            window.alert("Please connect to Metamask.");
        }
        // Modern dapp browsers...
        if (window.ethereum) {
            window.web3 = new Web3(ethereum);
            try {
                // Request account access if needed
                await ethereum.enable()
                // Acccounts now exposed
                web3.eth.sendTransaction({/* ... */ });
            } catch (error) {
                // User denied account access...
            }
        }
        // Legacy dapp browsers...
        else if (window.web3) {
            App.web3Provider = web3.currentProvider;
            window.web3 = new Web3(web3.currentProvider);
            // Acccounts always exposed
            web3.eth.sendTransaction({/* ... */ });
        }
        // Non-dapp browsers...
        else {
            console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
        }
    },
    loadAccount: async () => {
        // Set the current blockchain account
        App.account = web3.eth.accounts[0];
        var accountInterval = setInterval(function() {
            if (web3.eth.accounts[0] !== App.account) {
                App.account = web3.eth.accounts[0];
                App.render ();
            }
        }, 1000);
    },
    loadContract: async () => {
        // Create a JavaScript version of the smart contract
        const scm = await $.getJSON('SupplyChainManager.json');
        App.contracts.SupplyChainManager = TruffleContract(scm);
        App.contracts.SupplyChainManager.setProvider(App.web3Provider);
    
        // Hydrate the smart contract with values from the blockchain
        App.scm = await App.contracts.SupplyChainManager.deployed();
    },
    render: async () => {
        // Prevent double render
        if (App.loading) {
            return;
        }
    
        // Update app loading state
        App.setLoading(true);
    
        // Render Account
        $('#account').html(App.account);
        web3.eth.getBalance(App.account, (err, result) => {
            if (err) App.etherBalance = 0;
            else App.etherBalance = result.c [0];
            $('#etherBalance').html(App.etherBalance);
        });
    
        // Render Items
        // await App.renderTasks()
        await App.renderOwnItems ();
        await App.renderOutgoingItems ();
        await App.renderIncomingItems ();
        await App.renderTransferredItems ();
    
        // Update loading state
        App.setLoading(false);
    },

    renderOwnItems: async () => {
        var cleanTable = "<tr><th style=\"text-align: center; padding-right: 10px\">ID</th><th style=\"text-align: center; padding-right: 10px\">Name</th><th style=\"text-align: center; padding-right: 10px\">Owner</th><th style=\"text-align: center; padding-right: 10px\"># of transfers</th></tr><tr class=\"ownItemInfoRowTemplate\"><td class=\"itemInfoId\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoName\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoOwner\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoStates\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemStateButton\" style=\"text-align: center; padding-right: 10px\"></td></tr>";
        $('#ownItemInfoTable').html(cleanTable);
        var itemIds = await App.scm.getItemList (0);
        const $itemInfoTemplate =  $('.ownItemInfoRowTemplate');
        itemIds.forEach(async item => {
            var itemInfo = await App.scm.getItemInfo (item.c [0]);
            const itemId = itemInfo [0].c [0];
            const itemName = itemInfo [1];
            const itemStateCount = itemInfo [2].c [0];
            const itemOwner = itemInfo [3];
            
            const $newItemInfoTemplate = $itemInfoTemplate.clone();
            $newItemInfoTemplate.find('.itemInfoId').html (itemId);
            $newItemInfoTemplate.find('.itemInfoName').html (itemName);
            $newItemInfoTemplate.find('.itemInfoOwner').html (itemOwner);
            $newItemInfoTemplate.find('.itemInfoStates').html (itemStateCount);
            var viewTransfersButton = '<a href="javascript:void(0)" onClick="App.viewTransfers({itemId:'+itemId+', stateCount:'+itemStateCount+'})" class="btn-sm btn-primary permissionDeny pull-right" role="button" data-toggle="modal" data-target="#itemStatesModal">View Transfers</a>';
            $newItemInfoTemplate.find('.itemStateButton').html (viewTransfersButton);
            $('#ownItemInfoTable').append ($newItemInfoTemplate);
            
        });
    },

    renderOutgoingItems: async () => {
        var cleanTable = "<tr><th style=\"text-align: center; padding-right: 10px\">ID</th><th style=\"text-align: center; padding-right: 10px\">Name</th><th style=\"text-align: center; padding-right: 10px\">Owner</th><th style=\"text-align: center; padding-right: 10px\"># of transfers</th></tr><tr class=\"outgoingItemInfoRowTemplate\"><td class=\"itemInfoId\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoName\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoOwner\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoStates\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemStateButton\" style=\"text-align: center; padding-right: 10px\"></td></tr>";
        $('#outgoingItemInfoTable').html(cleanTable);
        var itemIds = await App.scm.getItemList (1);
        const $itemInfoTemplate =  $('.outgoingItemInfoRowTemplate');
        itemIds.forEach(async item => {
            var itemInfo = await App.scm.getItemInfo (item.c [0]);
            const itemId = itemInfo [0].c [0];
            const itemName = itemInfo [1];
            const itemStateCount = itemInfo [2].c [0];
            const itemOwner = itemInfo [3];
            
            const $newItemInfoTemplate = $itemInfoTemplate.clone();
            $newItemInfoTemplate.find('.itemInfoId').html (itemId);
            $newItemInfoTemplate.find('.itemInfoName').html (itemName);
            $newItemInfoTemplate.find('.itemInfoOwner').html (itemOwner);
            $newItemInfoTemplate.find('.itemInfoStates').html (itemStateCount);
            var viewTransfersButton = '<a href="javascript:void(0)" onClick="App.viewTransfers({itemId:'+itemId+', stateCount:'+itemStateCount+'})" class="btn-sm btn-primary permissionDeny pull-right" role="button" data-toggle="modal" data-target="#itemStatesModal">View Transfers</a>';
            $newItemInfoTemplate.find('.itemStateButton').html (viewTransfersButton);
            $('#outgoingItemInfoTable').append ($newItemInfoTemplate);
            
        });

    },

    renderIncomingItems: async () => {
        var cleanTable = "<tr><th style=\"text-align: center; padding-right: 10px\">ID</th><th style=\"text-align: center; padding-right: 10px\">Name</th><th style=\"text-align: center; padding-right: 10px\">Owner</th><th style=\"text-align: center; padding-right: 10px\"># of transfers</th></tr><tr class=\"incomingItemInfoRowTemplate\"><td class=\"itemInfoId\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoName\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoOwner\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoStates\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemStateButton\" style=\"text-align: center; padding-right: 10px\"></td></tr>";
        $('#incomingItemInfoTable').html(cleanTable);
        var itemIds = await App.scm.getItemList (2);
        const $itemInfoTemplate =  $('.incomingItemInfoRowTemplate');
        itemIds.forEach(async item => {
            var itemInfo = await App.scm.getItemInfo (item.c [0]);
            const itemId = itemInfo [0].c [0];
            const itemName = itemInfo [1];
            const itemStateCount = itemInfo [2].c [0];
            const itemOwner = itemInfo [3];
            
            const $newItemInfoTemplate = $itemInfoTemplate.clone();
            $newItemInfoTemplate.find('.itemInfoId').html (itemId);
            $newItemInfoTemplate.find('.itemInfoName').html (itemName);
            $newItemInfoTemplate.find('.itemInfoOwner').html (itemOwner);
            $newItemInfoTemplate.find('.itemInfoStates').html (itemStateCount);
            var viewTransfersButton = '<a href="javascript:void(0)" onClick="App.viewTransfers({itemId:'+itemId+', stateCount:'+itemStateCount+'})" class="btn-sm btn-primary permissionDeny pull-right" role="button" data-toggle="modal" data-target="#itemStatesModal">View Transfers</a>';
            $newItemInfoTemplate.find('.itemStateButton').html (viewTransfersButton);
            $('#incomingItemInfoTable').append ($newItemInfoTemplate);
        });

    },

    renderTransferredItems: async () => {
        var cleanTable = "<tr><th style=\"text-align: center; padding-right: 10px\">ID</th><th style=\"text-align: center; padding-right: 10px\">Name</th><th style=\"text-align: center; padding-right: 10px\">Owner</th><th style=\"text-align: center; padding-right: 10px\"># of transfers</th></tr><tr class=\"transferredItemInfoRowTemplate\"><td class=\"itemInfoId\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoName\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoOwner\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemInfoStates\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"itemStateButton\" style=\"text-align: center; padding-right: 10px\"></td></tr>";
        $('#transferredItemInfoTable').html(cleanTable);
        var itemIds = await App.scm.getItemList (3);
        const $itemInfoTemplate =  $('.transferredItemInfoRowTemplate');
        itemIds.forEach(async item => {
            var itemInfo = await App.scm.getItemInfo (item.c [0]);
            const itemId = itemInfo [0].c [0];
            const itemName = itemInfo [1];
            const itemStateCount = itemInfo [2].c [0];
            const itemOwner = itemInfo [3];
            
            const $newItemInfoTemplate = $itemInfoTemplate.clone();
            $newItemInfoTemplate.find('.itemInfoId').html (itemId);
            $newItemInfoTemplate.find('.itemInfoName').html (itemName);
            $newItemInfoTemplate.find('.itemInfoOwner').html (itemOwner);
            $newItemInfoTemplate.find('.itemInfoStates').html (itemStateCount);
            var viewTransfersButton = '<a href="javascript:void(0)" onClick="App.viewTransfers({itemId:'+itemId+', stateCount:'+itemStateCount+'})" class="btn-sm btn-primary permissionDeny pull-right" role="button" data-toggle="modal" data-target="#itemStatesModal">View Transfers</a>';
            $newItemInfoTemplate.find('.itemStateButton').html (viewTransfersButton);
            $('#transferredItemInfoTable').append ($newItemInfoTemplate);
        });

    },

    viewTransfers: async (obj) => { 
        // console.log (obj);
        // alert('got into the function: ' + obj.itemId + " state count = " + obj.stateCount);
        var cleanTable = "<tr><th style=\"text-align: center; padding-right: 10px\">Date</th><th style=\"text-align: center; padding-right: 10px\">Previous Owner</th><th style=\"text-align: center; padding-right: 10px\">Location</th><th style=\"text-align: center; padding-right: 10px\">Pending</th></tr><tr class=\"itemTransferInfoRowTemplate\"> <td class=\"transferDate\" style=\"text-align: center; padding-right: 10px\"></td> <td class=\"transferPreviousOwner\" style=\"text-align: center; padding-right: 10px\"></td><td class=\"transferLocation\" style=\"text-align: center; padding-right: 10px\"></td> <td class=\"transferPending\" style=\"text-align: center; padding-right: 10px\"></td></tr>";
        $('#itemTransfersInfoTable').html(cleanTable);
        const $itemInfoTemplate =  $('.itemTransferInfoRowTemplate');
        for (var i = 1; i <= obj.stateCount; i++) {
            var stateData = await App.scm.getStateInfo (obj.itemId, i);
            const transferDate = new Date (stateData [0].c [0] * 1000);
            const previousOwner = stateData [1];
            const location = stateData [3];
            const transferPending = stateData [2]? "No":"Yes";

            const $newItemInfoTemplate = $itemInfoTemplate.clone();
            $newItemInfoTemplate.find('.transferDate').html (transferDate);
            $newItemInfoTemplate.find('.transferPreviousOwner').html (previousOwner);
            $newItemInfoTemplate.find('.transferLocation').html (location);
            $newItemInfoTemplate.find('.transferPending').html (transferPending);
            $('#itemTransfersInfoTable').append ($newItemInfoTemplate);
        }
    },

    createUser: async () => {
        var $userName=$('#createUserName').val();
        var $userAddress=$('#createUserAddress').val();
        const output = await App.scm.createUser ($userAddress, $userName);
    },

    createItem: async () => {
        var $itemName=$('#createItemName').val();
        var $itemLocation=$('#createItemLocation').val();
        const output = await App.scm.createItem ($itemName, $itemLocation);
    },

    requestItemTransfer: async () => {
        var $itemId = $('#transferItemId').val();
        var $targetUser = $('#transferTargetUser').val();
        var $location = $('#transferTargetLocation').val();
        const output = await App.scm.requestItemTransfer ($itemId, $targetUser, $location);
    },

    acceptTransferRequest: async () => {
        var $itemId = $('#acceptItemId').val();
        var $sourceUser = $('#acceptSourceUser').val();
        const output = await App.scm.acceptItemTransfer ($itemId, $sourceUser);
    },

    rejectTransferRequest: async () => {
        var $itemId = $('#rejectItemId').val();
        var $sourceUser = $('#rejectSourceUser').val();
        const output = await App.scm.rejectItemTransfer ($itemId, $sourceUser);
    },


    watchNewItemCreatedEvent: async () => {
        var itemCreatedEvent = await App.scm.NewItemCreated({}, {
            fromBlock: 0,
            toBlock: 'latest'
        });
        itemCreatedEvent.watch (async (error, result) => {
            if (error) {
                console.log (error);
                // alert ("Unable to create item");
            }
            else {
                console.log (result);
                if (result.args._userAddress == App.account) {
                    // alert ("Item created. New ItemID = " + result.args._itemId.c [0]);
                    App.render ();
                }                
            }
        });
    },

    watchNewUserAdded: async () => {
        var userCreatedEvent = await App.scm.NewUserAdded({}, {
            fromBlock: 0,
            toBlock: 'latest'
        });
        userCreatedEvent.watch (async (error, result) => {
            if (error) {
                console.log (error);
                // alert ("Unable to create item");
            }
            else {
                console.log (result);
                var adminAddress = await App.scm.admin ();
                if (adminAddress == App.account) {
                    alert ("New user created.");
                    App.render ();
                }                
            }
        });

    },

    watchItemTransferRequested: async () => {
        var transferRequestedEvent = await App.scm.ItemTransferRequested({}, {
            fromBlock: 0,
            toBlock: 'latest'
        });
        transferRequestedEvent.watch (async (error, result) => {
            if (error) {
                console.log (error);
                // alert ("Unable to create item");
            }
            else {
                console.log (result);
                if (result.args._sourceUser == App.account || result.args._targetUser == App.account) {                    
                    App.render ();
                } 
            }
        });

    },

    watchItemTransferAccepted: async () => {
        var transferAcceptedEvent = await App.scm.ItemTransferAccepted({}, {
            fromBlock: 0,
            toBlock: 'latest'
        });
        transferAcceptedEvent.watch (async (error, result) => {
            if (error) {
                console.log (error);
                // alert ("Unable to create item");
            }
            else {
                console.log (result);
                if (result.args._sourceUser == App.account || result.args._targetUser == App.account) {
                    App.render ();
                } 
            }
        });
    },

    watchItemTransferRejected: async () => {
        var transferRejectedEvent = await App.scm.ItemTransferRejected({}, {
            fromBlock: 0,
            toBlock: 'latest'
        });
        transferRejectedEvent.watch (async (error, result) => {
            if (error) {
                console.log (error);
                // alert ("Unable to create item");
            }
            else {
                console.log (result);
                if (result.args._sourceUser == App.account || result.args._targetUser == App.account) {
                    App.render ();
                } 
            }
        });
    },






    setLoading: (boolean) => {
        App.loading = boolean
        const loader = $('#loader')
        const content = $('#content')
        if (boolean) {
            loader.show()
            content.hide()
        } else {
            loader.hide()
            content.show()
        }
    }
}

$(() => {
    $(window).load(() => {
        App.load()
        document.getElementById('btnCreateUser').onclick = App.createUser;
        document.getElementById('btnCreateItem').onclick = App.createItem;
        document.getElementById('btnTransferItem').onclick = App.requestItemTransfer;
        document.getElementById('btnAcceptItem').onclick = App.acceptTransferRequest;
        document.getElementById('btnRejectItem').onclick = App.rejectTransferRequest;
        
    });
})