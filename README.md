# PoC for blockchain based supply chain management system

## Requirements

### Deadline: 

11:59 PM July 21st, 2019

### Description

I want them to build a supply chain PoC in either Ethereum or Hyperledger. The application should be able to do the following:

* A user is able to create an item (some object, such as a string, is fine), and send it to another user for ownership

* A user is able to receive and send an item to another user

* A user can view the entire supply chain history as a list, such as whose currently the owner of the item, and the record of transferals

* **Bonus** – An administrator can create users dynamically

* **Bonus** – Showcase the application through a UI and not through command line.

### Tools and Software

The following need to be installed in your system

* Node.js and npm <https://nodejs.org/en/download/>

* Metamask browser plugin <https://metamask.io/>. (When you install Metamask in your browser, you will need to follow the instructions to create an account)

* Ganache <https://www.trufflesuite.com/ganache>

* Truffle (will be installed automatically in the setup steps)

### Running the project

1. Launch Ganache (you may use the quick settings to start with default settings).

2. Launch your browser and login to Metamask with the account you created during installation.

3. Unzip the project to your desired directory.

4. Navigate to the project directory in command prompt and run:

	`npm install`

	This will install all dependencies.

5. In the command prompt, run:

	`npm run compilesc`

	This will compile the smartcontract.

6. In the command prompt, run:

	`npm run migratesc`

	This will deploy the smart contract on your private blockchain that was created using Ganache.

7. In the command prompt, run:

	`npm run dev`

	This will launch the frontend application on your browser and you can test different functions of the smart contract.



## Plan

### Structs and Variables

```
address admin;
uint totalUserCount;
uint totalItemCount;

struct User {
	address accountAddress;
	string name;
	uint[] ownedItemIds;
	uint ownedItemCount;
	uint[] outgoingItemIds;
	uint outgoingItemCount;
	uint[] incomingItemIds;
	uint incomingItemCount;
	uint[] transferredItemIds;
	uint transferredItemCount;
	bool userExists;
}
address[] public userAddresses;
mapping (address => User) public userAddressToUser;

struct Item {
	uint itemId;
	string name;
	string description;
	State [] states;
	uint stateCount;
	bool itemExists;
	address owner;
}
mapping (uint => Item) public itemIdToItem;
uint[] public itemIds;

struct State {
	uint dateCreated;
	address previousOwner;
	bool accepted;
}
```

### Functions

1. 	Constructor -> admin

2. 	Admin can add users

3. 	Create user

	* When creating a new user, we can use the condition required (msg.sender == admin) to ensure only admin can add user

	* Set the userExists bool to true

	* Pass name

	* Pass address
    
4. 	Add item

	* Any user can create an item

	* When a user creates an item, they become its owner

5.	Send item

	* Item id + address to which you want to transfer

	* Check if item id exists

	* Check if you are the owner

	* Check if target address exists
    
	* Change nextOwner

	* Add item to incomingItems in target user struct

	* Increment incomingItemCount in target user struct

	* Add item to outgoingItems in the source user struct

	* Increment outgoingItemCount in the source user struct

	* STATE

		* create new state

		* set accepted to false

		* set previous owner to sender's address

		* set location to location received in input

		* add state to item's state array

		* increment stateCount

6.	Receive item

	* Repeatedly check your incoming items array

	* Check if item exists

	* Check item name

	* Check currentOwner of item

	* OnAccept

		* Change previous owner to source owner

		* Change current owner to target owner

		* Set nextOwner to null

		* Add item to ownedItems of target owner

		* Delete item from incomingItems of target owner

		* Decrement incomingItems count

		* Increment ownedItems count
			
		* Add item to transferredItems of source

		* Delete item from outgoingItems of source

		* Delete item from ownedItems of source

		* Increment transferredItemCount of source

		* decrement outgoingItemCount of source

		* decrement ownedItemCount of source

		* STATE

			* change accepted to true

	* OnReject

		* Remove item from target's incomingItems

		* Decrement target's incomingItemCount

		* Remove item from source's outgoingItems

		* Decrement source's outgoingItemCount

		* STATE

			* remove last state from item's state array

			* decrement stateCount

8. 	Show item list

9. 	Show item history

### Questions

#### How do we know if a user exists in the system or not?

added boolean in user struct. Otherwise, if you pass any address to the mapping address => user, it will return

### Notes

* If a transfer request is rejected, the item is moved from the owner's outgoing array to ownedItems array. There is no notification for rejected transfers. the only indication is that the item will be visible in the ownedItems.
Same case for successful transfers. The only indication of a successful transfer would be that the item will no longer be visible in the outgoing or owned items array

* DappUniversity - latest project

* Issue with Item.states being an array and assigning new states to it:

	UnimplementedFeatureError: Copying of type struct SupplyChainManager.State memory[] memory to storage not yet supported.

  	https://stackoverflow.com/questions/49345903/copying-of-type-struct-memory-memory-to-storage-not-yet-supported