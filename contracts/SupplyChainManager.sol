pragma solidity ^0.5.0;

contract SupplyChainManager {
    address public admin;
    uint public totalUserCount;
    uint public totalItemCount;

    struct User {
        address accountAddress;
        string name;
        uint[] ownedItemIds; //should these be mappings instead?
        uint[] outgoingItemIds;
        uint[] incomingItemIds;
        uint[] transferredItemIds;
        uint ownedItemCount;
        uint outgoingItemCount;
        uint incomingItemCount;
        uint transferredItemCount;
        bool userExists;
    }
    address[] public userAddresses;
    mapping (address => User) public userAddressToUser;

    struct Item {
        uint itemId;
        string name;
        // string description;
        // State[] states;
        mapping (uint => State) states;
        uint stateCount;
        bool itemExists;
        address owner;
    }
    mapping (uint => Item) public itemIdToItem;
    uint[] public itemIds;

    struct State {
        uint dateCreated;
        address previousOwner;
        bool accepted;
        string location;
    }

    modifier isAdmin () {
        require (msg.sender == admin, "Unauthorized");
        _;
    }

    modifier isUser (address _userAddress) {
        require (userAddressToUser [_userAddress].userExists, "User not registered");
        _;
    }

    modifier isNewUser (address _userAddress) {
        require (userAddressToUser [_userAddress].userExists == false, "User already exists");
        _;
    }

    modifier checkItemExists (uint _itemId) {
        require (itemIdToItem [_itemId].itemExists, "Item not found");
        _;
    }

    modifier checkItemOwner (uint _itemId) {
        require (itemIdToItem [_itemId].owner == msg.sender, "You must be the owner of the item to perform this action");
        _;
    }

    modifier checkItemOwnerForTransfer (uint _itemId, address _owner) {
        require (itemIdToItem [_itemId].owner == _owner, "Item not owned by sender");
        _;
    }

    modifier checkSelfTransfer (address _targetUser) {
        require (msg.sender != _targetUser, "Cannot transfer item to your own account");
        _;
    }

    modifier checkTransferrable (uint _itemId) {
        uint mId = 0;
        for (uint i = 0; i < userAddressToUser [msg.sender].ownedItemIds.length; i++) {
            if (userAddressToUser [msg.sender].ownedItemIds [i] == _itemId) {
                mId = userAddressToUser [msg.sender].ownedItemIds [i];
                break;
            }
        }
        require (mId > 0, "Item not available to transfer");
        _;
    }

    // modifier checkItemInSenderOutgoing (uint _itemId, address _sender) {
    //     uint mId = 0;
    //     for (uint i = 1; i < userAddressToUser [_sender].outgoingItemIds.length; i++) {
    //         if (userAddressToUser [_sender].outgoingItemIds [i] == _itemId) {
    //             mId = userAddressToUser [_sender].outgoingItemIds [i];
    //             break;
    //         }
    //     }
    //     require (mId > 0, "No transfer request found for this item");
    //     _;
    // }

    modifier checkItemInSenderOutgoing (uint _itemId) {
        uint mId = 0;
        for (uint i = 0; i < userAddressToUser [itemIdToItem [_itemId].owner].outgoingItemIds.length; i++) {
            if (userAddressToUser [itemIdToItem [_itemId].owner].outgoingItemIds [i] == _itemId) {
                mId = userAddressToUser [itemIdToItem [_itemId].owner].outgoingItemIds [i];
                break;
            }
        }
        require (mId > 0, "No transfer request found for this item");
        _;
    }

    modifier checkItemInReceiverIncoming (uint _itemId) {
        uint mId = 0;
        for (uint i = 0; i < userAddressToUser [msg.sender].incomingItemIds.length; i++) {
            if (userAddressToUser [msg.sender].incomingItemIds [i] == _itemId) {
                mId = userAddressToUser [msg.sender].incomingItemIds [i];
                break;
            }
        }
        require (mId > 0, "No transfer request found for this item");
        _;
    }

    modifier checkItemInOutgoingIncoming (uint _itemId) {
        uint mId = 0;
        for (uint i = 0; i < userAddressToUser [itemIdToItem [_itemId].owner].outgoingItemIds.length; i++) {
            if (userAddressToUser [itemIdToItem [_itemId].owner].outgoingItemIds [i] == _itemId) {
                mId += 1;
                break;
            }
        }
        for (uint i = 0; i < userAddressToUser [msg.sender].incomingItemIds.length; i++) {
            if (userAddressToUser [msg.sender].incomingItemIds [i] == _itemId) {
                mId += 1;
                break;
            }
        }
        require (mId == 2, "No transfer request found for this item");
        _;
    }

    // In the events below, sourceUser refers to the user who initiated the transfer
    // targetUser refers to the user who to which the transfer request was sent to
    event ItemTransferRequested (uint _itemId, address _targetUser, address _sourceUser);

    event ItemTransferAccepted (uint _itemId, address _targetUser, address _sourceUser);

    event ItemTransferRejected (uint _itemId, address _targetUser, address _sourceUser);

    event NewUserAdded (address _userAddress);

    event NewItemCreated (address _userAddress, uint _itemId);

    constructor () public {
        admin = msg.sender;
        totalUserCount = 0;
        totalItemCount = 0;
        // TODO should i register admin as a user here
        createUser (msg.sender, "Admin");
    }

    function createUser (address _userAddress, string memory _name) public
    isAdmin ()
    isNewUser (_userAddress) {
        // User memory nUser = userAddressToUser [_userAddress];

        // create an instance of the user struct
        User memory nUser;
        // set user flag to true
        nUser.userExists = true;
        // set user's name
        nUser.name = _name;
        // set user's account address
        nUser.accountAddress = _userAddress;
        // initialize ownedItemCount
        nUser.ownedItemCount = 0;
        // initialize outgoingItemCount
        nUser.outgoingItemCount = 0;
        // initialize incomingItemCount
        nUser.incomingItemCount = 0;
        // initialize transferredItemCount
        nUser.transferredItemCount = 0;
        // add user to user mapping
        userAddressToUser [_userAddress] = nUser;
        // add user's address to the user list
        userAddresses.push (_userAddress);
        // increment total user count
        totalUserCount += 1;
    }

    function createItem (string memory _name, string memory _location) public
    isUser (msg.sender) {
        // create an instance of the item struct
        Item memory nItem;
        // increment total item count. This will be used as the item id
        totalItemCount += 1;
        // push item id to the item id list
        itemIds.push (totalItemCount);
        // set item instance's id
        nItem.itemId = totalItemCount;
        // set item flag to true
        nItem.itemExists = true;
        // set item name
        nItem.name = _name;
        // set item's owner
        nItem.owner = msg.sender;
        // initialize item's sate count
        nItem.stateCount = 0;

        // create an instance of the state struct
        State memory nState;
        // set the creation date of the state
        nState.dateCreated = now;
        // set the previous owner of this state to null
        nState.previousOwner = address (0);
        // set the state flag as true
        nState.accepted = true;
        // set the state location
        nState.location = _location;

        // add item to item mapping
        itemIdToItem [nItem.itemId] = nItem;
        // increment state count
        itemIdToItem [nItem.itemId].stateCount += 1;
        // add state to item
        itemIdToItem [nItem.itemId].states [itemIdToItem [nItem.itemId].stateCount] = nState;

        // add item to user's owned item list
        userAddressToUser [msg.sender].ownedItemIds.push (itemIdToItem [nItem.itemId].itemId);
        // increment owned item count
        userAddressToUser [msg.sender].ownedItemCount += 1;

        // broadcast item creation
        emit NewItemCreated (msg.sender, nItem.itemId);
    }

    /* requestItemTransfer
    * Add item to incomingItems in target user struct
    * Increment incomingItemCount in target user struct
    * Add item to outgoingItems in the source user struct
    * Increment outgoingItemCount in the source user struct
    * STATE
        * create new state
        * set accepted to false
        * set previous owner to sender's address
        * set location to location received in input
        * add state to item's state array
        * increment stateCount
    */
    function requestItemTransfer (uint _itemId, address _targetUser, string memory _location) public
    isUser (msg.sender)
    checkItemExists (_itemId)
    checkItemOwner (_itemId)
    checkTransferrable (_itemId)
    checkSelfTransfer (_targetUser)
    isUser (_targetUser) {
        // add item to target user's incoming items
        userAddressToUser [_targetUser].incomingItemIds.push (_itemId);
        // increment target user's incoming item count
        userAddressToUser [_targetUser].incomingItemCount += 1;
        // add item to source user's outgoing items
        userAddressToUser [msg.sender].outgoingItemIds.push (_itemId);
        // increment source user's outgoing item count
        userAddressToUser [msg.sender].outgoingItemCount += 1;

        // TODO should the item be removed from the owner's owned items list here or when the transfer request is accepted?
        // remove item from source user's owned items list
        // int index = findIndexInList (_itemId, userAddressToUser [msg.sender].ownedItemIds);
        int index = findIndexInList (_itemId, msg.sender, 0);
        if (index >= 0) {
            // userAddressToUser [msg.sender].ownedItemIds = removeFromList (uint (index), userAddressToUser [msg.sender].ownedItemIds);
            removeFromList (uint (index), msg.sender, 0);
            // decrement source user's owned item count
            userAddressToUser [msg.sender].ownedItemCount -= 1;
        }

        // // create an instance of the state struct
        // State memory nState;
        // // set state's creation date
        // nState.dateCreated = now;
        // // set source user as previous owner
        // nState.previousOwner = msg.sender;
        // // set state's accepted flag to false to show that this transfer has not been accepted
        // nState.accepted = false;
        // // set location of the state (for location tracking etc.)
        // nState.location = _location;

        // // itemIdToItem [_itemId].states.push (nState); // When Item.state was an array

        // increment item's state count
        itemIdToItem [_itemId].stateCount += 1;
        // set state's creation date
        itemIdToItem [_itemId].states [itemIdToItem [_itemId].stateCount].dateCreated = now;
        // set source user as previous owner
        itemIdToItem [_itemId].states [itemIdToItem [_itemId].stateCount].previousOwner = msg.sender;
        // set state's accepted flag to false to show that this transfer has not been accepted
        itemIdToItem [_itemId].states [itemIdToItem [_itemId].stateCount].accepted = false;
        // set location of the state (for location tracking etc.)
        itemIdToItem [_itemId].states [itemIdToItem [_itemId].stateCount].location = _location;

        // // add state to item
        // itemIdToItem [_itemId].states [itemIdToItem [_itemId].stateCount] = nState;

        // broadcast transfer request
        emit ItemTransferRequested (_itemId, _targetUser, msg.sender);
    }

    /* acceptItemTransfer
    * Change item owner to target owner
    * Add item to ownedItems of target owner
    * Increment ownedItems count
    * Delete item from incomingItems of target owner
    * Decrement incomingItems count
    * Add item to transferredItems of source
    * Increment transferredItemCount of source

    * Delete item from outgoingItems of source
    * decrement outgoingItemCount of source

    // TODO should i do this in the transfer request function instead
    * Delete item from ownedItems of source
    * decrement ownedItemCount of source
    * STATE
        * change accepted to true
    */
    function acceptItemTransfer (uint _itemId, address _sourceUser) public
    isUser (msg.sender)
    isUser (_sourceUser)
    checkSelfTransfer (_sourceUser)
    checkItemExists (_itemId)
    checkItemOwnerForTransfer (_itemId, _sourceUser)
    // checkItemInSenderOutgoing (_itemId, _sourceUser)
    // checkItemInSenderOutgoing (_itemId)
    checkItemInOutgoingIncoming (_itemId)
    // checkItemInReceiverIncoming (_itemId)
    {
        // change item's owner to target user
        itemIdToItem [_itemId].owner = msg.sender;

        // add item to owned items of target user
        userAddressToUser [msg.sender].ownedItemIds.push (_itemId);
        // increment owned item count
        userAddressToUser [msg.sender].ownedItemCount += 1;

        // delete item from incoming item list of target user
        // int index = findIndexInList (_itemId, userAddressToUser [msg.sender].incomingItemIds);
        int index = findIndexInList (_itemId, msg.sender, 2);
        // TODO if index < 0, should i return false or continue with the rest of the process
        if (index >= 0) {
            // userAddressToUser [msg.sender].incomingItemIds = removeFromList (uint(index), userAddressToUser [msg.sender].incomingItemIds);
            removeFromList (uint(index), msg.sender, 2);
            // decrement target user's incoming item count
            userAddressToUser [msg.sender].incomingItemCount -= 1;
        }

        // add item to source user's transferred items
        userAddressToUser [_sourceUser].transferredItemIds.push (_itemId);
        // increment source user's transferred item count
        userAddressToUser [_sourceUser].transferredItemCount += 1;

        // delete item from source user's outgoing item list
        // index = findIndexInList (_itemId, userAddressToUser [_sourceUser].outgoingItemIds);
        index = findIndexInList (_itemId, _sourceUser, 1);
        if (index >= 0) {
            // userAddressToUser [_sourceUser].outgoingItemIds = removeFromList (uint (index),  userAddressToUser [_sourceUser].outgoingItemIds);
            removeFromList (uint (index),  _sourceUser, 1);
            // decrement source user's outgoing item count
            userAddressToUser [_sourceUser].outgoingItemCount -= 1;
        }
        // change state flag to true
        // itemIdToItem [_itemId].states [itemIdToItem [_itemId].stateCount - 1].accepted = true; // When Item.state was an array
        itemIdToItem [_itemId].states [itemIdToItem [_itemId].stateCount].accepted = true;
        emit ItemTransferAccepted (_itemId, msg.sender, _sourceUser);
    }

    /* rejectItemTransfer
    * Remove item from target's incoming items
    * Decrement target's incoming item count
    * Remove item from source's outgoing items
    * Decrement source's outgoing item count
    * Add item to source's owned items
    * Increment source's owned item count
    * STATE
        * remove last state from item's state array
        * decrement stateCount
    */
    function rejectItemTransfer (uint _itemId, address _sourceUser) public
    isUser (msg.sender)
    isUser (_sourceUser)
    checkSelfTransfer (_sourceUser)
    checkItemExists (_itemId)
    checkItemOwnerForTransfer (_itemId, _sourceUser)
    // checkItemInSenderOutgoing (_itemId, _sourceUser)
    // checkItemInSenderOutgoing (_itemId)
    checkItemInOutgoingIncoming (_itemId)
    // checkItemInReceiverIncoming (_itemId)
    {
        // Remove item from target's incoming items
        // int index = findIndexInList (_itemId, userAddressToUser [msg.sender].incomingItemIds);
        int index = findIndexInList (_itemId, msg.sender, 2);
        if (index >= 0) {
            // userAddressToUser [msg.sender].incomingItemIds = removeFromList (uint (index), userAddressToUser [msg.sender].incomingItemIds);
            removeFromList (uint (index), msg.sender, 2);
            // Decrement target's incoming item count
            userAddressToUser [msg.sender].incomingItemCount -= 1;
        }
        // Remove item from source's outgoing items
        // index = findIndexInList (_itemId, userAddressToUser [_sourceUser].outgoingItemIds);
        index = findIndexInList (_itemId, _sourceUser, 1);
        if (index >= 0) {
            // userAddressToUser [_sourceUser].outgoingItemIds = removeFromList (uint (index), userAddressToUser [_sourceUser].outgoingItemIds);
            removeFromList (uint (index), _sourceUser, 1);
            // Decrement source's outgoing item count
            userAddressToUser [_sourceUser].outgoingItemCount -= 1;
        }
        // Add item to source's owned items
        userAddressToUser [_sourceUser].ownedItemIds.push (_itemId);
        // Increment source's owned item count
        userAddressToUser [_sourceUser].ownedItemCount += 1;

        // remove last state from item's state array
        // itemIdToItem [_itemId].states.pop (); // When Item.state was an array
        delete itemIdToItem [_itemId].states [itemIdToItem [_itemId].stateCount];
        // decrement item's state count
        itemIdToItem [_itemId].stateCount -= 1;

        //broadcast request rejection
        emit ItemTransferRejected (_itemId, msg.sender, _sourceUser);
    }

    /* getItemList
     * Gets a list of items for the user
     * Which list to get is determined by the code passed to the function
     * code 0 = owned items
     * code 1 = outgoing items
     * code 2 = incoming items
     * code 3 = transferred items
    */
    function getItemList (uint _code) public view
    isUser (msg.sender)
    returns (uint[] memory _itemList) {
        if (_code == 0) return userAddressToUser [msg.sender].ownedItemIds;
        else if (_code == 1) return userAddressToUser [msg.sender].outgoingItemIds;
        else if (_code == 2) return userAddressToUser [msg.sender].incomingItemIds;
        else if (_code == 3) return userAddressToUser [msg.sender].transferredItemIds;
    }

    /* getUserItemList
    * Only for admin. Allows the admin to get a list of any user's item
    * Which list to get is determined by the code passed to the function
    * code 0 = owned items
    * code 1 = outgoing items
    * code 2 = incoming items
    * code 3 = transferred items
    */
    function getUserItemList (uint _code, address _userAddress) public view
    isUser (msg.sender)
    isAdmin()
    returns (uint[] memory _itemList) {
        if (_code == 0) return userAddressToUser [_userAddress].ownedItemIds;
        else if (_code == 1) return userAddressToUser [_userAddress].outgoingItemIds;
        else if (_code == 2) return userAddressToUser [_userAddress].incomingItemIds;
        else if (_code == 3) return userAddressToUser [_userAddress].transferredItemIds;
    }

    /* getItemInfo
    * Returns item's id, name, state count, owner (current)
    * The item must belong to the user
    */
    function getItemInfo (uint _itemId) public view
    checkItemExists (_itemId)
    isUser (msg.sender)
    // checkItemOwner (_itemId)
    returns (uint itemId, string memory name, uint stateCount, address owner) {
        return (itemIdToItem [_itemId].itemId,
        itemIdToItem [_itemId].name,
        itemIdToItem [_itemId].stateCount,
        itemIdToItem [_itemId].owner);
    }

    /* getItemInfoAdmin
    * For admin only
    * Allows admin to get any item's id, name, state count, owner (current)
    */
    function getItemInfoAdmin (uint _itemId) public view
    checkItemExists (_itemId)
    isUser (msg.sender)
    isAdmin ()
    returns (uint itemId, string memory name, uint stateCount, address owner) {
        return (itemIdToItem [_itemId].itemId,
        itemIdToItem [_itemId].name,
        itemIdToItem [_itemId].stateCount,
        itemIdToItem [_itemId].owner);
    }

    /* getStateInfo
    * Returns item's state's creation date, previous owner, accepted flag, location
    * The item must belong to the user
    */
    function getStateInfo (uint _itemId, uint _stateIndex) public view
    checkItemExists (_itemId)
    isUser (msg.sender)
    // checkItemOwner (_itemId)
    returns (uint dateCreated, address previousOwner, bool accepted, string memory location) {
        return (itemIdToItem [_itemId].states [_stateIndex].dateCreated,
        itemIdToItem [_itemId].states [_stateIndex].previousOwner,
        itemIdToItem [_itemId].states [_stateIndex].accepted,
        itemIdToItem [_itemId].states [_stateIndex].location);
    }

    /* getStateInfoAdmin
    * For admin only
    * Allows admin to get any item's state's creation date, previous owner, accepted flag, location
    */
    function getStateInfoAdmin (uint _itemId, uint _stateIndex) public view
    checkItemExists (_itemId)
    isUser (msg.sender)
    isAdmin ()
    returns (uint dateCreated, address previousOwner, bool accepted, string memory location) {
        return (itemIdToItem [_itemId].states [_stateIndex].dateCreated,
        itemIdToItem [_itemId].states [_stateIndex].previousOwner,
        itemIdToItem [_itemId].states [_stateIndex].accepted,
        itemIdToItem [_itemId].states [_stateIndex].location);
    }

    // function findIndexInList (uint _itemId, uint[] memory _list) private pure returns (int index) {
    //     for (uint i = 0; i < _list.length; i++) {
    //         if (_list [i] == _itemId) {
    //             return int (i);
    //         }
    //     }
    //     return -1;
    // }

    /*
     * code 0 = owned items
     * code 1 = outgoing items
     * code 2 = incoming items
     * code 3 = transferred items
    */
    function findIndexInList (uint _itemId, address _userAddress, uint _code) private view returns (int index) {
        if (_code == 0) {
            for (uint i = 0; i < userAddressToUser [_userAddress].ownedItemIds.length; i++) {
                if (userAddressToUser [_userAddress].ownedItemIds [i] == _itemId) {
                    return int (i);
                }
            }
        } else if (_code == 1) {
            for (uint i = 0; i < userAddressToUser [_userAddress].outgoingItemIds.length; i++) {
                if (userAddressToUser [_userAddress].outgoingItemIds [i] == _itemId) {
                    return int (i);
                }
            }
        } else if (_code == 2) {
            for (uint i = 0; i < userAddressToUser [_userAddress].incomingItemIds.length; i++) {
                if (userAddressToUser [_userAddress].incomingItemIds [i] == _itemId) {
                    return int (i);
                }
            }
        } else if (_code == 3) {
            for (uint i = 0; i < userAddressToUser [_userAddress].transferredItemIds.length; i++) {
                if (userAddressToUser [_userAddress].transferredItemIds [i] == _itemId) {
                    return int (i);
                }
            }
        }
        return -1;
    }

    // function removeFromList (uint _index, uint[] memory _list) private pure returns (uint[] memory _nList) {
    //     for (uint i = _index; i < _list.length - 1; i++) {
    //         _list [i] = _list [i + 1];
    //     }
    //     return _list;
    // }

    function removeFromList (uint _index, address _userAddress, uint _code) private {
        if (_code == 0) {
            for (uint i = _index; i < userAddressToUser [_userAddress].ownedItemIds.length - 1; i++) {
                userAddressToUser [_userAddress].ownedItemIds [i] = userAddressToUser [_userAddress].ownedItemIds [i + 1];
            }
            userAddressToUser [_userAddress].ownedItemIds.pop();
        } else if (_code == 1) {
            for (uint i = _index; i < userAddressToUser [_userAddress].outgoingItemIds.length - 1; i++) {
                userAddressToUser [_userAddress].outgoingItemIds [i] = userAddressToUser [_userAddress].outgoingItemIds [i + 1];
            }
            userAddressToUser [_userAddress].outgoingItemIds.pop();
        } else if (_code == 2) {
            for (uint i = _index; i < userAddressToUser [_userAddress].incomingItemIds.length - 1; i++) {
                userAddressToUser [_userAddress].incomingItemIds [i] = userAddressToUser [_userAddress].incomingItemIds [i + 1];
            }
            userAddressToUser [_userAddress].incomingItemIds.pop();
        } else if (_code == 3) {
            for (uint i = _index; i < userAddressToUser [_userAddress].transferredItemIds.length - 1; i++) {
                userAddressToUser [_userAddress].transferredItemIds [i] = userAddressToUser [_userAddress].transferredItemIds [i + 1];
            }
            userAddressToUser [_userAddress].transferredItemIds.pop();
        }
    }
}